Trabajo Práctico 4

1. Se tiene un arreglo desordenado de enteros en donde se sabe que los valores almacenados
pertenecen al intervalo [0, 9999]. Implementar un algoritmo que lo ordene con complejidad
temporal O(N). ¿Qué ocurre con la eficiencia de este algoritmo para arreglos de pocos elementos?
2. Implementar el algoritmo quick sort. Escribir un programa que muestre el tiempo requerido para
ordenar un arreglo desordenado, uno que ya está ordenado ascendentemente y uno que está
ordenado descendentemente. Verificar experimentalmente con arreglos de distintos tamaños el
orden de complejidad temporal para cada caso.
3. Escribir un programa que implemente el algoritmo selection sort y permita verificar de manera
empírica su orden de complejidad temporal, ordenando arreglos de distintos tamaños.
4. A continuación se muestra una posible implementación del algoritmo merge sort. Analizar la
complejidad espacial y realizar los cambios necesarios para lograr que sea O(N).
    
    ```java
    public static <E> void sort(List<E> elements, Comparator<? super E> comparator) {
        if (elements.size() <= 1) {
            return;
        }
        int n = elements.size() / 2;
        List<E> left = new ArrayList<E>(elements.subList(0, n));
        List<E> right = new ArrayList<E>(elements.subList(n, elements.size()));
        sort(left, comparator);
        sort(right, comparator);
        merge(left, right, elements, comparator);
    }
    
    private static <E> void merge(List<E> left, List<E> right, List<E> result,
    Comparator<? super E> cmp) {
        int i1 = 0, i2 = 0, i = 0;
        while (i1 < left.size() || i2 < right.size()) {
            if (i2 == right.size() || (i1 < left.size() &amp;&amp;
                cmp.compare(left.get(i1), right.get(i2)) <= 0)) {
                result.set(i++, left.get(i1++));
            } else {
                result.set(i++, right.get(i2++));
            }
        }
    }
    ```

    ¿Hay algún caso en el que la complejidad temporal de esta implementación no sea O(N log N)? De
    ser así, realizar las modificaciones necesarias para que siempre lo sea.

5. El algoritmo insertion sort consiste en iterar por los elementos de un arreglo, garantizando en
cada paso que los elementos recorridos hasta el momento se encuentran ordenados. Es decir, en
cada iteración el nuevo elemento es insertado en el lugar que le corresponda entre los ya recorridos
hasta el momento. Al terminar el ciclo el arreglo se encuentra ordenado.
Realizar una implementación de este algoritmo, determinar su orden de complejidad temporal y
verificarlo experimentalmente.

6. Una estrategia para insertar un registro en un archivo ordenado consiste en insertar el elemento al
final del mismo y luego ordenarlo con algún método. Si tuviera que elegir entre selection sort,
insertion sort o bubble sort, ¿cuál escogería y por qué?

        Elgiria Insertion sort porque esta literalmente en el nombre del algoritmo.
        Agarra los elementos todavia no ordenados del final y los inserta en la position correcta