package com.eda.itba;

public class InsertionSort {
    public static int[] sort(int[] unorderedArray) {
        System.out.println("Insertionsorting an array of size " + unorderedArray.length + "...");
        long start = System.currentTimeMillis();
        insertionSort(unorderedArray);
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Done! Total duration: " + duration + "ms.");
        return unorderedArray;
    }

    public static void insertionSort(int[] unorderedArray) {
        for (int i = 1; i < unorderedArray.length; i++) {
            int toInsertValue = unorderedArray[i];
            int j = i-1;
            while(j>=0 && toInsertValue < unorderedArray[j]) {
                unorderedArray[j+1] = unorderedArray[j];
                j--;
            }
            unorderedArray[j+1] = toInsertValue;
        }
    }
}
