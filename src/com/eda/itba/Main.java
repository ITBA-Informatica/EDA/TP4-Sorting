package com.eda.itba;

import com.sun.scenario.effect.Merge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int[] unorderedArray = Utils.randomPositiveArray(30);
        List<Integer> intList = new ArrayList<>();
        for (int i : unorderedArray)
        {
            intList.add(i);
        }
        intList = MergeSort.sort(intList,(a,b) -> a - b);
        System.out.println(intList);

//        System.out.println("Unsorted array");
//        int[] unorderedArray = Utils.randomPositiveArray(200);
//        System.out.println(Arrays.toString(unorderedArray));
//        InsertionSort.sort(unorderedArray);
//        System.out.println(Arrays.toString(unorderedArray));
//        System.out.println();
//
//        System.out.println("Unsorted array");
//        unorderedArray = Utils.randomPositiveArray(200);
//        System.out.println(Arrays.toString(unorderedArray));
//        SelectionSort.sort(unorderedArray);
//        System.out.println(Arrays.toString(unorderedArray));
//        System.out.println();
////
//        System.out.println("Unsorted array");
//        unorderedArray = Utils.randomPositiveArray(200);
//        System.out.println(Arrays.toString(unorderedArray));
//        int[] sortedArray = QuickSort.sort(unorderedArray);
//        System.out.println(Arrays.toString(sortedArray));
//        System.out.println();

//        System.out.println("Sorted array");
//        unorderedArray = Utils.randomSortedArray(300000);
//        sortedArray = QuickSort.sort(unorderedArray);
//
//        System.out.println();
//        System.out.println("Reverse-Sorted array");
//        unorderedArray = Utils.randomSortedArray(300000);
//        int length = unorderedArray.length;
//        int[] reversedArray = new int[length];
//        for (int i = 0; i < length; i++) {
//            reversedArray[i] = unorderedArray[length-(i+1)];
//        }
//        QuickSort.sort(reversedArray);
    }
}
