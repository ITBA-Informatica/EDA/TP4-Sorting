package com.eda.itba;

public class IntegerSort {



    public static int[] sort(int[] unorderedArray) {

//        Creo como una especie de hash en donde el valor es la cantidad de veces que tiene el array desordenado ese valor
//        Lo malo de esto es que estoy creando un array de 10.000 lugares y despues recorriendolo solo para
//        ordenar unos 5 numeros quizas

        int[] intCount = new int[10000];
        for (int i = 0; i < unorderedArray.length; i++) {
            intCount[unorderedArray[i]] += 1;
        }
        int[] orderedArray = new int[unorderedArray.length];
        int j = 0;
        for (int i = 0; i < intCount.length; i++) {
            while(intCount[i] != 0) {
                orderedArray[j] = i;
                j++;
                intCount[i] --;
            }
        }
        return orderedArray;
    }
}
