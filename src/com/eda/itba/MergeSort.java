package com.eda.itba;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MergeSort {
    public static <E> List<E> sort(List<E> elements, Comparator<? super E> comparator) {
        System.out.println("Selectionsorting a list of size " + elements.size()+ "...");
        long start = System.currentTimeMillis();
        List<E> newList = new ArrayList<>(elements);
        mergeSort(elements, 0,elements.size() - 1,comparator, newList);
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Done! Total duration: " + duration + "ms.");
        elements = newList;
        return newList;
    }

    public static <E> void mergeSort(List<E> elements, int start, int end, Comparator<? super E> comparator, List<E> orderededElements) {
//        System.out.println("Start: " + start + ". End: " + end);

        if (end - start + 1 <= 1) {
            return;
        }

        int n = (start + end) / 2;
        mergeSort(elements, start, n, comparator, orderededElements);
        mergeSort(elements, (n+1), end, comparator, orderededElements);
        merge(elements, start, end, comparator, orderededElements);
    }

    private static <E> void merge(List<E> elements, int start, int end,Comparator<? super E> cmp, List<E> orderededElements) {
        int n = (start + end) / 2;
        int i1 = start;
        int i2 = n+1;
        int newPos = start;
        while (i1 <= n || i2 <= end) {
            if (i2 > end) {
                while(i1 <= n) {
                    orderededElements.set(newPos, elements.get(i1));
                    i1++;
                    newPos++;
                }
            } else if (i1 > n) {
                while(i2 <= end) {
                    orderededElements.set(newPos, elements.get(i2));
                    i2++;
                    newPos++;
                }
            }else {
                if (cmp.compare(elements.get(i1),elements.get(i2)) <= 0) {
                    orderededElements.set(newPos, elements.get(i1));
                    i1++;
                    newPos++;
                } else {
                    orderededElements.set(newPos, elements.get(i2));
                    i2++;
                    newPos++;
                }
            }
        }

        for (int i = start; i <= end; i++) {
            elements.set(i,orderededElements.get(i));
        }

    }
}
