package com.eda.itba;

import java.util.Arrays;

public class QuickSort {

    public static double avgPos = 0;
    public static int avgCount = 0;

    public static int[] sort(int[] unorderedArray) {
        avgCount=0;
        avgPos = 0;
        System.out.println("QuickSorting an array of size " + unorderedArray.length + "...");
        long start = System.currentTimeMillis();
        int[] orderedArray = quicksort(unorderedArray, 0, unorderedArray.length - 1);
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Done! Total duration: " + duration + "ms.");
        System.out.println("Average pivot position: " + (avgPos / avgCount));
        return orderedArray;
    }

    public static int[] quicksort(int[] unorderedArray, int start, int end) {
        if ((end - start + 1) <= 1) {
            return unorderedArray;
        }

        int pivotPos = (int) (Math.random() * (end-start+1)) + start;
        avgPos += (pivotPos - start) * 1.0 / (end-start);
        avgCount++;
        int pivot = unorderedArray[pivotPos];

//        Pongo el pivot primero
        int aux = unorderedArray[start];
        unorderedArray[start] = pivot;
        unorderedArray[pivotPos] = aux;


        int k = start;
        for (int i = start + 1; i <= end; i++) {
            if (unorderedArray[i] < pivot) {
                k++;
                aux = unorderedArray[k];
                unorderedArray[k] = unorderedArray[i];
                unorderedArray[i] = aux;

            }
        }
        aux = unorderedArray[k];
        unorderedArray[k] = unorderedArray[start];
        unorderedArray[start] = aux;
        quicksort(unorderedArray,start,k - 1);
        quicksort(unorderedArray,k+1,end);
        return unorderedArray;

    }
}
