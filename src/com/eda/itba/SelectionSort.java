package com.eda.itba;

public class SelectionSort {
    public static int[] sort(int[] unorderedArray) {
        System.out.println("Selectionsorting an array of size " + unorderedArray.length + "...");
        long start = System.currentTimeMillis();
        selectionSort(unorderedArray);
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Done! Total duration: " + duration + "ms.");
        return unorderedArray;
    }

    public static void selectionSort(int[] unorderedArray) {
        for (int i = 0; i < unorderedArray.length; i++) {
            int min = i;
            int j = i + 1;
            while(j < unorderedArray.length) {
                if (unorderedArray[j] < unorderedArray[min]) {
                    min = j;
                }
                j++;
            }
            if (i != min) {
                    int aux = unorderedArray[i];
                    unorderedArray[i] = unorderedArray[min];
                    unorderedArray[min] = aux;
            }
        }
    }
}